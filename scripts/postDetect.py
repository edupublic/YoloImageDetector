import os
import shutil

from lief.ELF import OS_ABI


def postDetect():
    imDir = './yolov5/runs/detect'
    dirs = filter(lambda x: os.path.isdir(imDir + '/' + x), os.listdir(imDir))
    for directory in dirs:
        for file in os.listdir(imDir + '/' + directory):
            filePath = imDir + '/' + directory + '/' + file
            shutil.copy(filePath, imDir + '/' + file)
            os.remove(filePath)
            pass
        os.rmdir(imDir + '/' + directory)
        pass
    pass


postDetect()
