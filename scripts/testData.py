import os


def dataForTest(imgDir="../Data/test/images"):
    list = os.listdir(imgDir)
    absList = []
    for file in list:
        absList.append(os.path.abspath("../Data/test/images/" + file))
        pass
    return absList


print(dataForTest())
print(len(dataForTest()))

files = dataForTest()

for position, file in enumerate(files):
    print('now --> ' + str(position) + ' / ' + str(len(files) - 1))
    wPath = './yolov5/runs/yolov5_trained/weights/best.pt'
    os.system('python ./yolov5/detect.py --weights ' + wPath + ' --source ' + file + ' --img 1920')
    pass
