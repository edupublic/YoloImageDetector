import random
import shutil
from pathlib import Path
import os

allDataPath = '../Data/Where_Waldo'
imagesPath = allDataPath + '/images'
labelsPath = allDataPath + '/labels'

files = os.listdir(imagesPath)
print(files)

multiplier = 0.6
trainCount = int(int(len(files) * multiplier))
testCount = len(files) - trainCount

trainData = list(map(lambda x: x.replace('.jpg', ''), random.sample(files, trainCount)))
testData = list(map(lambda x: x.replace('.jpg', ''), list(set(files) - set(trainData))))

mainDirst = [
    '../Data/train/images',
    '../Data/train/labels',
    '../Data/valid/images',
    '../Data/valid/labels'
]

for directory in mainDirst:
    Path(directory).mkdir(parents=True, exist_ok=True)
    pass

for file in trainData:
    imgSrc = imagesPath + '/' + file + '.jpg'
    lbsSrc = labelsPath + '/' + file + '.txt'
    imgDst = '../Data/train/images/' + file + '.jpg'
    lbsDst = '../Data/train/labels/' + file + '.txt'

    try:
        shutil.copy(lbsSrc, lbsDst)
        shutil.copy(imgSrc, imgDst)
    except FileNotFoundError:
        continue
    pass

for file in testData:
    imgSrc = imagesPath + '/' + file + '.jpg'
    lbsSrc = labelsPath + '/' + file + '.txt'
    imgDst = '../Data/valid/images/' + file + '.jpg'
    lbsDst = '../Data/valid/labels/' + file + '.txt'

    try:
        shutil.copy(lbsSrc, lbsDst)
        shutil.copy(imgSrc, imgDst)
    except FileNotFoundError:
        continue
    pass

print(len(os.listdir("../Data/train/images")))
print(len(os.listdir("../Data/train/labels")))

print(len(os.listdir("../Data/valid/images")))
print(len(os.listdir("../Data/valid/labels")))
